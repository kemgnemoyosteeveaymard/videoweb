var sessionName;
var token;
var exitingSessionName;
var baseUrl = "https://api.testedoctar.ml";
if((/^http\:\/\/localhost/.test(window.location.href))|| (/^http\:\/\/192/.test(window.location.href))){
  baseUrl = 'https://localhost:1337'; // for running/testing locally
}

const user = randomString();
const ovSettings = {
  chat: true,
  autopublish: true,
  toolbarButtons: {
    audio: true,
    video: true,
    screenShare: true,
    fullscreen: true,
    exit: true
  }
};


$(document).ready(() => { 

  var webComponent = document.querySelector("openvidu-webcomponent");
  var form = document.getElementById("main");

  webComponent.addEventListener("joinSession", event => {    
    console.log('join session');
  });

  webComponent.addEventListener("leaveSession", event => {
    form.style.display = "block";
    webComponent.style.display = "none";
    var path = (location.pathname.slice(-1) == "/" ? location.pathname : location.pathname + "/");
    window.history.pushState("", "", path);
  });

  webComponent.addEventListener("error", event => {
    console.log("Error event", event.detail);
  });

  exitingSessionName = window.location.hash.slice(1); // For 'https://myurl/#roomId', sessionId would be 'roomId'
  console.log(exitingSessionName);
	if (!isEmpty(exitingSessionName)) {
		// The URL has a session id. Join the room right away
		console.log("Joining to room " + exitingSessionName);		
		joinExistingSession(exitingSessionName);
	}
});

function joinExistingSession(exitingSessionName) {  
  var form = document.getElementById("main");
  var webComponent = document.querySelector("openvidu-webcomponent");

  form.style.display = "none";
  webComponent.style.display = "block";

  getTokenFromExistingSession(response => {        
    token = response.token;
    
    console.log(response);
    webComponent.sessionConfig = { exitingSessionName, user, token, ovSettings};
    
  });

  return false;
}

function testCorsAction(){
  testCors(baseUrl + "/api-sessions/get-token", "POST", (response) => {
    console.log("testCorsResponse", response);
  })
}

function joinSession() {
  //var sessionName = document.getElementById('sessionName').value;
  //var user = document.getElementById('user').value;  
  var form = document.getElementById("main");
  var webComponent = document.querySelector("openvidu-webcomponent");

  form.style.display = "none";
  webComponent.style.display = "block";

  getToken(response => {    
    console.log(response);
    sessionName = response.sessionName;
    token = response.token;
    
    console.log(response);
    webComponent.sessionConfig = { sessionName, user, token,ovSettings};
    var path = (location.pathname.slice(-1) == "/" ? location.pathname : location.pathname + "/");
    console.log(sessionName);
		window.history.pushState("", "", path + '#' + sessionName);
  });

  return false;
}

/**
 * --------------------------
 * SERVER-SIDE RESPONSIBILITY
 * --------------------------
 */

function getToken(callback) {
  //sessionName = $("#sessionName").val(); // Video-call chosen by the user

  httpPostRequest(
    baseUrl + "/api-sessions/get-token",
    { info: "sessionName" },
    "Request of TOKEN gone WRONG:",
    response => {            
      console.log(response);
      console.warn("Request of TOKEN gone WELL (TOKEN:" + response.token + ")");
      callback({ token: response.token, sessionName: response.sessionName }); // Continue the join operation
    }
  );
}

function getTokenFromExistingSession(callback){
  httpPostRequest(
    baseUrl + "/api-sessions/get-token",
    { sessionName: exitingSessionName },
    "Request of TOKEN gone WRONG:",
    response => {      
      console.warn("Request of TOKEN gone WELL (TOKEN:" + response.token + ")");
      callback({ token: response.token, sesionName: response.sessionName }); // Continue the join operation
    }
  );
}

function httpPostRequest(url, body, errorMsg, callback) {
  var http = new XMLHttpRequest();
  http.open("POST", url, true);
  http.setRequestHeader("Content-type", "application/json");
  http.addEventListener("readystatechange", processRequest, false);
  http.send(JSON.stringify(body));

  function processRequest() {
    if (http.readyState == 4) {
      if (http.status == 200) {
        try {
          callback(JSON.parse(http.responseText));
        } catch (e) {
          callback();
        }
      } else {
        console.warn(errorMsg);
        console.warn(http.responseText);
      }
    }
  }
}

function randomString() {
  return Math.random()
    .toString(36)
    .slice(2);
}

function testCors(url, method, callback){
  $.ajax({
    url:url,
    method: method,
    success: (data, state, res) => {
      if(data){
        console.log(data);
        console.log(state);
        callback(data);
      }else{
        console.log(res.status)
      }
    },
    error: (data, state)=> {
      console.log(state);
      console.log(data);
    }
  })
}

function isEmpty(elt){    
  if(typeof elt !== "string" || elt.trim().length === 0){        
      return true;
  }
}
